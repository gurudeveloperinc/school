<?php

namespace App\Http\Controllers;

 use App\course;
use App\dept;
use App\lecturer;
use App\program;
use App\result;
use App\resulthistory;
 use App\staff;
 use App\students;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PHPExcel_IOFactory;

class AdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('admin');
	}

//		----------- STUDENT BEGINS ----------
	 public function addStudents(){
		 return view('admin.addStudents');
	 }

	 public function postAddStudents(Request $request) {

//		$password = Str::random(6);

		 $students = new students();
		 $students->fName = $request->input('fName');
		 $students->sName = $request->input('sName');
		 $students->oName = $request->input('oName');
		 $students->dob = $request->input('dob');
		 $students->userName = $request->input('userName');
		 $students->password = $request->input('password');
		 $students->class = $request->input('class');
		 $students->classExtension = $request->input('classExtension');
		 $students->club = $request->input('club');
		 $students->house = $request->input('house');
		 $students->gender = $request->input('gender');
		 $students->stateOfOrigin = $request->input('stateOfOrigin');
		 $students->email = $request->input('email');
		 $students->nextOfKin = $request->input('nextOfKin');
		 $students->nextOfKinPhone = $request->input('nextOfKinPhone');
		 $status =	$students->save();

		 if($status)
			 $request->session()->flash('success','Student added successfully');
		 else
			 $request->session()->flash('error','Something went wrong. Please try again');

		 return redirect ('/admins/add-students');

//		mail($request->input('email'),"Your regex app credentials", "You have successfully been added to the regex app system
//		for result retrieval. Please download the regex app from the app store. Login with your ID number and this password: $password" );
//		if($status) $report = "Student added successfully!"; else $report = "Sorry an error occured";
//		return view('admin.addStudents', ['programmes' => $programmes, 'status' => $report]);
	 }

	 public function postBulkAddStudents( Request $request ) {

		 try {


			 $inputFileName = $request->file('file')->getClientOriginalName();
			 $request->file('file')->move("uploads/admins/students",$inputFileName);

			 /* Identify file, create reader and load file  */
			 $inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/admins/students/".$inputFileName);
			 $objReader = PHPExcel_IOFactory::createReader($inputFileType);
			 $objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/admins/students/".$inputFileName);

			 $sheet = $objPHPExcel->getSheet(0);
			 $highestRow = $sheet->getHighestRow();
			 $highestColumn = $sheet->getHighestColumn();

			 $time_pre = microtime(true);

			 //  Read a row of data into an array
			 $rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				 NULL, TRUE, FALSE);


			 $count=1;


			 // add results to data base from file
			 foreach($rowData as $cell){

//				 $programme = program::where('progname',$cell[9])->get();

//				 $password = Str::random(6);

				 $students = new students();
				 $students->fName = $cell[0];
				 $students->sNamw = $cell[1];
				 $students->oName = $cell[2];
				 $students->dob = $cell[3];
				 $students->userNmae = $cell[4];
				 $students->password = $cell[5];
				 $students->phone = $cell[6];
				 $students->class = $cell[7];
				 $students->club = $cell[8];
				 $students->house = $cell[9];
				 $students->stateOfOrigin = $cell[10];
				 $students->email = $cell[11];
				 $students->nextOfKin = $cell[11];
				 $students->nextOfKinPhone = $cell[11];
				 $students->save();

//				 mail($cell[4],"Your regex app credentials", "You have successfully been added to the regex app system for result retrieval. Please download the regex app from the app store. Login with your ID number and this password: $password" );

			 }


		 }
		 catch (Exception $e) {
			 die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
				 . '": ' . $e->getMessage());
		 }


	 }

	 public function getViewStudents()
	 {
		 return view('admin.viewStudents');
	 }

	 public function getViewStudentsByClass(){
		 $students = students::all();

		 $primaryOne = 0;
		 $primaryTwo = 0;
		 $primaryThree = 0;
		 $primaryFour = 0;
		 $primaryFive = 0;
		 $primarySix = 0;

		 $primaryOnes = array();
		 $primaryTwos = array();
		 $primaryThrees = array();
		 $primaryFours = array();
		 $primaryFives = array();
		 $primarySixs = array();

			 foreach($students as $item){
				 if($item->class == "Primary 1"){
					 $primaryOne++;
					 array_push($primaryOnes,$item);
				 }
				 if($item->class == "Primary 2"){
					 $primaryTwo++;
					 array_push($primaryTwos,$item);
				 }
				 if($item->class == "Primary 3"){
					 $primaryThree++;
					 array_push($primaryThrees,$item);
				 }
				 if($item->class == "Primary 4"){
					 $primaryFour++;
					 array_push($primaryFours,$item);
				 }
				 if($item->class == "Primary 5"){
					 $primaryFive++;
					 array_push($primaryFives,$item);
				 }
				 if($item->class == "Primary 6"){
					 $primarySix++;
					 array_push($primarySixs,$item);
				 }

			 }

		 return view('admin.viewStudentsByClass',[
			 'students' => $students,
			 'primaryOne' => $primaryOne,
			 'primaryTwo' => $primaryTwo,
			 'primaryThree' => $primaryThree,
			 'primaryFour' => $primaryFour,
			 'primaryFive' => $primaryFive,
			 'primarySix' => $primarySix,
			 'primaryOnes' => $primaryOnes,
			 'primaryTwos' => $primaryTwos,
			 'primaryThrees' => $primaryThrees,
			 'primaryFours' => $primaryFours,
			 'primaryFives' => $primaryFives,
			 'primarySixs' => $primarySixs

		 ]);
	 }

	 public function getViewStudentsInPrimaryOne(){
		 $students = students::all();

		 $primaryOneA = array();
		 $primaryOneB = array();
		 $primaryOneC = array();

		 foreach($students as $item){

			 if($item->classExtension == "A" && $item->class == "Primary 1"){
				 array_push($primaryOneA,$item);
			 }
			 if($item->classExtension == "B" && $item->class == "Primary 1"){
				 array_push($primaryOneB,$item);
			 }
			 if($item->classExtension == "C" && $item->class == "Primary 1"){
				 array_push($primaryOneC,$item);
			 }
		 }

		 return view('admin.primaryOne',[
			 'primaryOneA' => $primaryOneA,
			 'primaryOneB' => $primaryOneB,
			 'primaryOneC' => $primaryOneC
		 ]);
	 }

	 public function getViewStudentsInPrimaryTwo(){
		 $students = students::all();

		 $primaryTwoA = array();
		 $primaryTwoB = array();
		 $primaryTwoC = array();

		 foreach($students as $item){


			 if($item->classExtension == "A" && $item->class == "Primary 2"){
				 array_push($primaryTwoA,$item);
			 }
			 if($item->classExtension == "B" && $item->class == "Primary 2"){
				 array_push($primaryTwoB,$item);
			 }
			 if($item->classExtension == "C" && $item->class == "Primary 2"){
				 array_push($primaryTwoC,$item);
			 }
		 }

		 return view('admin.primaryTwo',[

			 'primaryTwoA' => $primaryTwoA,
			 'primaryTwoB' => $primaryTwoB,
			 'primaryTwoC' => $primaryTwoC
		 ]);
	 }

	 public function getViewStudentsInPrimaryThree(){
		 $students = students::all();

		 $primaryThreeA = array();
		 $primaryThreeB = array();
		 $primaryThreeC = array();

		 foreach($students as $item){


			 if($item->classExtension == "A" && $item->class == "Primary 3"){
				 array_push($primaryThreeA,$item);
			 }
			 if($item->classExtension == "B" && $item->class == "Primary 3"){
				 array_push($primaryThreeB,$item);
			 }
			 if($item->classExtension == "C" && $item->class == "Primary 3"){
				 array_push($primaryThreeC,$item);
			 }
		 }

		 return view('admin.primaryThree',[

			 'primaryThreeA' => $primaryThreeA,
			 'primaryThreeB' => $primaryThreeB,
			 'primaryThreeC' => $primaryThreeC
		 ]);
	 }

	 public function getViewStudentsInPrimaryFour(){
		 $students = students::all();

		 $primaryFourA = array();
		 $primaryFourB = array();
		 $primaryFourC = array();

		 foreach($students as $item){


			 if($item->classExtension == "A" && $item->class == "Primary 4"){
				 array_push($primaryFourA,$item);
			 }
			 if($item->classExtension == "B" && $item->class == "Primary 4"){
				 array_push($primaryFourB,$item);
			 }
			 if($item->classExtension == "C" && $item->class == "Primary 4"){
				 array_push($primaryFourC,$item);
			 }
		 }

		 return view('admin.primaryFour',[

			 'primaryFourA' => $primaryFourA,
			 'primaryFourB' => $primaryFourB,
			 'primaryFourC' => $primaryFourC,
		 ]);
	 }

	 public function getViewStudentsInPrimaryFive(){
		 $students = students::all();

		 $primaryFiveA = array();
		 $primaryFiveB = array();
		 $primaryFiveC = array();

		 foreach($students as $item){


			 if($item->classExtension == "A" && $item->class == "Primary 5"){
				 array_push($primaryFiveA,$item);
			 }
			 if($item->classExtension == "B" && $item->class == "Primary 6"){
				 array_push($primaryFiveB,$item);
			 }
			 if($item->classExtension == "C" && $item->class == "Primary 6"){
				 array_push($primaryFiveC,$item);
			 }
		 }

		 return view('admin.primaryFive',[

			 'primaryFiveA' => $primaryFiveA,
			 'primaryFiveB' => $primaryFiveB,
			 'primaryFiveC' => $primaryFiveC
		 ]);
	 }

	 public function getViewStudentsInPrimarySix(){
		 $students = students::all();

		 $primarySixA = array();
		 $primarySixB = array();
		 $primarySixC = array();

		 foreach($students as $item){


			 if($item->classExtension == "A" && $item->class == "Primary 6"){
				 array_push($primarySixA,$item);
			 }
			 if($item->classExtension == "B" && $item->class == "Primary 6"){
				 array_push($primarySixB,$item);
			 }
			 if($item->classExtension == "C" && $item->class == "Primary 6"){
				 array_push($primarySixC,$item);
			 }
		 }

		 return view('admin.primarySix',[

			 'primarySixA' => $primarySixA,
			 'primarySixB' => $primarySixB,
			 'primarySixC' => $primarySixC
		 ]);
	 }

	 public function getAddStaff(){
		 return view('admin.addStaff');
	 }

	 public function postAddStaff (Request $request) {


		 $staff = new staff();
		 $staff->fName = $request->input('fName');
		 $staff->sName = $request->input('sName');
		 $staff->oName = $request->input('oName');
		 $staff->email = $request->input('email');
		 $staff->userName = $request->input('userName');
		 $staff->password = $request->input('password');
		 $staff->phone = $request->input('phone');
		 $staff->department = $request->input('department');
		 $staff->maritalStatus = $request->input('maritalStatus');
		 $staff->dob = $request->input('dob');
		 $staff->address = $request->input('address');
		 $staff->gender = $request->input('gender');
		 $staff->stateOfOrigin = $request->input('stateOfOrigin');
		 $staff->nextOfKin = $request->input('nextOfKin');
		 $staff->nextOfKinPhone = $request->input('nextOfKinPhone');
		 $status =	$staff->save();

		 if($status)
			 $request->session()->flash('success','Student added successfully');
		 else
			 $request->session()->flash('error','Something went wrong. Please try again');

		 return redirect ('/admins/add-staff');

	 }

	 public function getViewStaff(){
		 $staff = staff::all();

		 $finance = 0;
		 $bursar = 0;
		 $ict = 0;
		 $hr = 0;
		 $academic = 0;

		 $finances = array();
		 $bursars = array();
		 $icts = array();
		 $hrs = array();
		 $academics = array();

		 foreach($staff as $item){
			 if($item->department == "Finance"){
				 $finance++;
				 array_push($finances,$item);
			 }
			 if($item->department == "Bursar"){
				 $bursar++;
				 array_push($bursars,$item);
			 }
			 if($item->department == "ICT"){
				 $ict++;
				 array_push($icts,$item);
			 }
			 if($item->department == "HR"){
				 $hr++;
				 array_push($hrs,$item);
			 }
			 if($item->department == "academic"){
				 $academic++;
				 array_push($academics,$item);
			 }
		 }

		 return view('admin.viewStaff',[
			 'staff' => $staff,
			 'finance' => $finance,
			 'bursar' => $bursar,
			 'ict' => $ict,
			 'hr' => $hr,
			 'academic' => $academic,
			 'finances' => $finances,
			 'bursars' => $bursars,
			 'icts' => $icts,
			 'hrs' => $hrs,
			 'academics' => $academics
		 ]);
	 }



 //		----------- STUDENT ENDS ----------


//	public function getIndex(){
//		$courseCount = course::all()->count();
//		$studentCount = student::all()->count();
//		$lecturerCount = lecturer::all()->count();
//		$resultsCount = result::all()->count();
//
//		$sietStudents = $this->getStudentsByFaculty(1);
//		$sblStudents = $this->getStudentsByFaculty(2);
//		$fasStudents = $this->getStudentsByFaculty(3);
//
//
//		return view('admin.home',[
//			'courseCount' => $courseCount,
//			'studentCount' => $studentCount,
//			'lecturerCount' => $lecturerCount,
//			'resultsCount' => $resultsCount,
//			'sietStudents' => $sietStudents,
//			'sblStudents' => $sblStudents,
//			'fasStudents' => $fasStudents
//		]);
//	}

	public function getIndex(){
		return view('admin.home');
	}

	public function getSettings(){
		return view('admin.settings');
	}

	public function messages(){
		return view('admin.messages');
	}

//	public function messages(){
//
//		$lecturers = lecturer::all();
//		return view('lecturers.messages',[
//			'lecturers' => $lecturers
//		]);
//	}

	public function getStudentsByFaculty( $fid ) {
		$sietDepts = dept::where('fid',$fid)->get();
		$sietDeptArray = array();


		foreach($sietDepts as $item){
			array_push($sietDeptArray,$item->did);
		}


		$sietProgs = program::whereIn('did',$sietDeptArray)->get();
		$sietProgKeys = array();
		foreach($sietProgs as $item){
			array_push($sietProgKeys,$item->progid);
		}


		$sietStudents = student::whereIn('progid',$sietProgKeys)->get();

		return $sietStudents;
	}

//	public function changeRoles() {
//		$lecturers = lecturer::all();
//
//		return view('admin.changeRoles', ['lecturers' => $lecturers ]);
//	}

	public function changeRoles(){
		return view('admin.changeRoles');
	}

//	public function updateUsers() {
//
//		$lecturers = lecturer::all();
//		$students = student::all();
//
////		if(Input::has('lSearch')){
////			$search = Input::get('search');
////			$search = '%' . $search . '%';
////
////			$lecturers = lecturer::where('name', 'like' , $search )->get();
////
////		}
//
////		if(Input::has('sSearch')){
////			$search = Input::get('search');
////			$students = $students->Where('name', 'like', '%' . $search . '%');
////
////		}
//
//		if( Input::has('sLevel') ) {
//
//			$level = Input::get('sLevel');
//			$students = $students->where('level',$level);
//
//		}
//
//		if(Input::has('sProg')) {
//			$prog     = Input::get( 'sProg' );
//			$students = $students->where( 'progid', $prog );
//		}
//
//		if(Input::has('lFaculty')){
//			$faculty = Input::get('lFaculty');
//
//			$filtered = collect();
//
//			foreach($lecturers as $item){
//				if($item->Dept->Faculty->name == $faculty){
//					$filtered->push($item);
//				}
//
//				$lecturers = $filtered;
//			}
//
//
//
//		}
//		if(Input::has('lRole')){
//			$role = Input::get('lRole');
//			$lecturers = $lecturers->where('role',$role);
//
//		}
//		if(Input::has('lDept')){
//			$dept = Input::get('lDept');
//			$lecturers = $lecturers->where('did',$dept);
//		}
//
//
//
//
//		return view('admin.updateUsers', ['lecturers' => $lecturers, 'students' => $students ]);
//	}

	public function updateUsers(){
		return view('admin.updateUsers');
	}

//	public function addStudents() {
//
//		$programmes = program::all();
//
//		return view('admin.addStudents', ['programmes' => $programmes]);
//	}

	
	public function postChangeRoles( Request $request ) {

		$lecturer = lecturer::find($request->input('lid'));
		$lecturer->role = $request->input('role');
		$lecturer->save();
	}

	public function postRollSemester(Request $request) {
		try {


			$inputFileName = $request->file('file')->getClientOriginalName();
			$request->file('file')->move("uploads/admins/rolled",$inputFileName);

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/admins/rolled/".$inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/admins/rolled/".$inputFileName);

			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			$time_pre = microtime(true);

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, TRUE, FALSE);


			$count=1;



			foreach($rowData as $cell){


				$student = student::where('studentid',$cell[0])->first();

				if($student->level ==  "PRE") {

					$student->level = 100;
					$student->save();

					mail($student->email,"Regent University - New level Assignment", "Congratulations $student->fname,\nYou have been moved a year ahead in the RUCST Results Portal." );

				}

				else if($student->level ==  "100") {
					$student->level = 200;
					$student->save();

					mail($student->email,"Regent University - New level Assignment", "Congratulations $student->fname,\nYou have been moved a year ahead in the RUCST Results Portal." );

				}

				else if($student->level ==  "200") {
					$student->level = 300;
					$student->save();

					mail($student->email,"Regent University - New level Assignment", "Congratulations $student->fname,\nYou have been moved a year ahead in the RUCST Results Portal." );

				}

				else if($student->level ==  "300") {
					$student->level = 400;
					$student->save();

					mail($student->email,"Regent University - New level Assignment", "Congratulations $student->fname,\nYou have been moved a year ahead in the RUCST Results Portal." );

				}

				else if($student->level ==  "400"){


					$results = result::where('sid',$student->sid)->get();

					foreach($results as $item ){
						$resultHistory = new resulthistory();
						$resultHistory->studentid = $student->studentid;
						$resultHistory->fname = $student->othernames;
						$resultHistory->sname = $student->surname;
						$resultHistory->phone = $student->phone;
						$resultHistory->email = $student->email;
						$resultHistory->staffID = $item->Lecturer->staffid;
						$resultHistory->lecturerName = $item->Lecturer->name;
						$resultHistory->courseName = $item->Course->name;
						$resultHistory->courseSemester = $item->Course->semester;
						$resultHistory->courseLevel = $item->Course->semester;
						$resultHistory->attendance = $item->attendance;
						$resultHistory->midsem = $item->midsem;
						$resultHistory->ca = $item->ca;
						$resultHistory->examscore = $item->examscore;
						$resultHistory->totalgrade = $item->totalgrade;
						$resultHistory->save();
					}
					$student->delete();

					mail($student->email,"Regent University - Graduation Note", "Congratulations $student->fname,\nYou have completed requirements for your degree. You are now an alumni. You would be moved to the alumni system. Thank you.\nIT Dept" );


				} // end 400 level code


			}


		}
		catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
				. '": ' . $e->getMessage());
		}


	}

}
