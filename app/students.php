<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class students extends Model
{
    //
    protected $primaryKey = "sid";

    public $table = "students";
}
