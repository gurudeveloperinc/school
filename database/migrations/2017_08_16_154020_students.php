<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Students extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

//		add gender to the db
//        make username firstname.lastname

        Schema::create('students', function (Blueprint $table) {
            $table->increments('sid');
            $table->string('fName');
            $table->string('sName');
            $table->string('oName')->nullable();
            $table->string('email')->nullable();
            $table->string('userName')->unique();
            $table->string('password');
            $table->string('dob');
            $table->string('stateOfOrigin');
            $table->enum( 'class', [ 'Primary 1', 'Primary 2', 'Primary 3', 'Primary 4', 'Primary 5', 'Primary 6' ] );
            $table->enum( 'classExtension', [ 'A', 'B', 'C'] );
            $table->string('club')->nullable();
            $table->enum( 'house', [ 'blue', 'green', 'yellow', 'red' ] )->nullable();
            $table->enum( 'gender', [ 'male', 'female' ] )->nullable();
            $table->string('nextOfKin');
            $table->string('nextOfKinPhone');


            $table->timestamps();
        });

        Schema::create('staff', function (Blueprint $table) {
            $table->increments('stid');
            $table->string('fName');
            $table->string('sName');
            $table->string('oName')->nullable();
            $table->string('email')->nullable();
            $table->string('userName')->unique();
            $table->string('password');
            $table->string('phone');
            $table->enum( 'department', [ 'Finance', 'Bursar', 'ICT', 'HR', 'Academic' ] );
            $table->enum( 'maritalStatus', [ 'Married', 'Single', 'Divorced', 'Widowed' ] );
            $table->string('dob');
            $table->string('address');
            $table->enum( 'gender', [ 'male', 'female' ] )->nullable();
            $table->string('stateOfOrigin');
            $table->string('nextOfKin');
            $table->string('nextOfKinPhone');


            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('students');
        Schema::drop('staff');
    }
}
