<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//admin routes

Route::get('/admins', 'AdminController@getIndex');

Route::get('/admins/add-students', 'AdminController@addStudents');
Route::get('/admins/view-students', 'AdminController@getViewStudents');
Route::get('/admins/view-students-class', 'AdminController@getViewStudentsByClass');
Route::get('/admins/view-students-in-PrimaryOne', 'AdminController@getViewStudentsInPrimaryOne');
Route::get('/admins/view-students-in-PrimaryTwo', 'AdminController@getViewStudentsInPrimaryTwo');
Route::get('/admins/view-students-in-PrimaryThree', 'AdminController@getViewStudentsInPrimaryThree');
Route::get('/admins/view-students-in-PrimaryFour', 'AdminController@getViewStudentsInPrimaryFour');
Route::get('/admins/view-students-in-PrimaryFive', 'AdminController@getViewStudentsInPrimaryFive');
Route::get('/admins/view-students-in-PrimarySix', 'AdminController@getViewStudentsInPrimarySix');
Route::post('/admins/add-students', 'AdminController@postAddStudents');
Route::post('/admins/bulk-add-students', 'AdminController@postBulkAddStudents');

Route::get('/admins/change-roles', 'AdminController@changeRoles');
Route::get('/admins/settings','AdminController@getSettings');
Route::get('/admins/messages','AdminController@messages');
Route::post('/admins/change-roles', 'AdminController@postChangeRoles');
Route::post('/admins/roll-semester','AdminController@postRollSemester');

Route::get('/admins/add-staff', 'AdminController@getAddStaff');
Route::post('/admins/add-staff','AdminController@postAddStaff');
Route::get('/admins/view-staff', 'AdminController@getViewStaff');
Route::get('/admins/update-users', 'AdminController@updateUsers');
