@extends('layouts.app')

@section('content')

    @include('sidebars.admin')


    <div class="row">

        <div class="col m10 right">
            <ul class="tabs tabs-transparent uploadOptions">
                <li class="tab"><a class="active" href="#finance">Finance <span class="noOfStudents">{{$finance}}</span></a></li>
                <li class="tab"><a href="#bursar">Bursar <span class="noOfStudents">{{$bursar}}</span></a></li>
                <li class="tab"><a href="#ict">ICT<span class="noOfStudents">{{$ict}}</span></a></li>
                <li class="tab"><a href="#hr">HR <span class="noOfStudents">{{$hr}}</span></a></li>
                <li class="tab"><a href="#academic">Academic<span class="noOfStudents">{{$academic}}</span></a></li>
            </ul>
        </div>
    </div>

    <div class="formBody">
        <div id="finance" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">
                        <span class="card-title">Finance Staff</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Phone</th>
                                <th class="center-align">department</th>
                                <th class="center-align">gender</th>
                            </tr>
                            @foreach($finances as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->sName}} {{$item->oName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->phone}}</td>
                                    <td class="center-align">{{$item->department}}</td>
                                    <td class="center-align">{{$item->gender}}</td>

                            @endforeach

                        </table>
                    </div>
                </div>
            </main>

        </div>

        <div id="bursar" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">
                        <span class="card-title">Bursar Staff</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Phone</th>
                                <th class="center-align">department</th>
                                <th class="center-align">gender</th>
                            </tr>
                            @foreach($bursars as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->sName}} {{$item->oName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->phone}}</td>
                                    <td class="center-align">{{$item->department}}</td>
                                    <td class="center-align">{{$item->gender}}</td>

                            @endforeach

                        </table>
                    </div>
                </div>
            </main>

        </div>

        <div id="ict" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">
                        <span class="card-title">ICT Staff</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Phone</th>
                                <th class="center-align">department</th>
                                <th class="center-align">gender</th>
                            </tr>
                            @foreach($icts as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->sName}} {{$item->oName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->phone}}</td>
                                    <td class="center-align">{{$item->department}}</td>
                                    <td class="center-align">{{$item->gender}}</td>

                            @endforeach

                        </table>
                    </div>
                </div>
            </main>

        </div>

        <div id="hr" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">
                        <span class="card-title">HR Staff</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Phone</th>
                                <th class="center-align">department</th>
                                <th class="center-align">gender</th>
                            </tr>
                            @foreach($hrs as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->sName}} {{$item->oName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->phone}}</td>
                                    <td class="center-align">{{$item->department}}</td>
                                    <td class="center-align">{{$item->gender}}</td>

                            @endforeach

                        </table>
                    </div>
                </div>
            </main>

        </div>

        <div id="academic" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">
                        <span class="card-title">Academic Staff</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Phone</th>
                                <th class="center-align">department</th>
                                <th class="center-align">gender</th>
                            </tr>
                            @foreach($academics as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->sName}} {{$item->oName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->phone}}</td>
                                    <td class="center-align">{{$item->department}}</td>
                                    <td class="center-align">{{$item->gender}}</td>

                            @endforeach

                        </table>
                    </div>
                </div>
            </main>

        </div>


    </div>


@endsection