@extends('layouts.app')

@section('content')

@include('sidebars.admin')


    <div class="row">

        <div class="col m10 right">
            <ul class="tabs tabs-transparent uploadOptions">
                <li class="tab"><a class="active" href="#primary1">Primary One <span class="noOfStudents">{{$primaryOne}}</span></a></li>
                <li class="tab"><a href="#primary2">Primary Two <span class="noOfStudents">{{$primaryTwo}}</span></a></li>
                <li class="tab"><a href="#primary3">Primary Three <span class="noOfStudents">{{$primaryThree}}</span></a></li>
                <li class="tab"><a href="#primary4">Primary Four <span class="noOfStudents">{{$primaryFour}}</span></a></li>
                <li class="tab"><a href="#primary5">Primary Five <span class="noOfStudents">{{$primaryFive}}</span></a></li>
                <li class="tab"><a href="#primary6">Primary Six <span class="noOfStudents">{{$primarySix}}</span></a></li>
            </ul>
        </div>

    </div>


    <div class="formBody">

        <div id="primary1" class="mn-content fixed-sidebar">
            <main class="mn-inner">
                <div  class="card upload" >

                    <br>
                    <span class="mytabs1"><a href="{{url('/admins/view-students-in-PrimaryOne')}}">A</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryOne')}}#primary1B">B</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryOne')}}#primary1C">C</a></span>

                    <div class="card-content">
                        <span class="card-title">Primary 1</span><br>
                            <table class="bordered table-striped">
                                <tr>
                                    <th class="center-align">Name</th>
                                    <th class="center-align">Email</th>
                                    <th class="center-align">Club</th>
                                    <th class="center-align">House</th>
                                    <th class="center-align">Gender</th>
                                </tr>
                                @foreach($primaryOnes as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->oName}} {{$item->sName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->club}}</td>
                                    <td class="center-align">{{$item->house}}</td>
                                    <td class="center-align">{{$item->gender}}</td>
                                </tr>
                                @endforeach
                            </table>
                    </div>
                </div>
            </main>
        </div>

        <div id="primary2" class="mn-content fixed-sidebar">
            <main class="mn-inner">
                <div  class="card upload" >

                    <br>
                    <span class="mytabs1"><a href="{{url('/admins/view-students-in-PrimaryTwo')}}">A</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryTwo')}}#primary2B">B</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryTwo')}}#primary2C">C</a></span>

                    <div class="card-content">
                        <span class="card-title">Primary 2</span><br>
                            <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Club</th>
                                <th class="center-align">House</th>
                                <th class="center-align">Gender</th>
                            </tr>
                            @foreach($primaryTwos as $item)
                            <tr>
                                <td class="center-align">{{$item->fName}} {{$item->oName}} {{$item->sName}}</td>
                                <td class="center-align">{{$item->email}}</td>
                                <td class="center-align">{{$item->club}}</td>
                                <td class="center-align">{{$item->house}}</td>
                                <td class="center-align">{{$item->gender}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </main>
        </div>

        <div id="primary3" class="mn-content fixed-sidebar">
            <main class="mn-inner">
                <div  class="card upload" >

                    <br>
                    <span class="mytabs1"><a href="{{url('/admins/view-students-in-PrimaryThree')}}">A</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryThree')}}#primary3B">B</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryThree')}}#primary3C">C</a></span>

                    <div class="card-content">
                        <span class="card-title">Primary 3</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Club</th>
                                <th class="center-align">House</th>
                                <th class="center-align">Gender</th>
                            </tr>
                            @foreach($primaryThrees as $item)
                            <tr>
                                <td class="center-align">{{$item->fName}} {{$item->oName}} {{$item->sName}}</td>
                                <td class="center-align">{{$item->email}}</td>
                                <td class="center-align">{{$item->club}}</td>
                                <td class="center-align">{{$item->house}}</td>
                                <td class="center-align">{{$item->gender}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </main>
        </div>

        <div id="primary4" class="mn-content fixed-sidebar">
            <main class="mn-inner">
                <div  class="card upload" >

                    <br>
                    <span class="mytabs1"><a href="{{url('/admins/view-students-in-PrimaryFour')}}">A</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryFour')}}#primary4B">B</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryFour')}}#primary4C">C</a></span>

                    <div class="card-content">
                        <span class="card-title">Primary 4</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Club</th>
                                <th class="center-align">House</th>
                                <th class="center-align">Gender</th>
                            </tr>
                            @foreach($primaryFours as $item)
                            <tr>
                                <td class="center-align">{{$item->fName}} {{$item->oName}} {{$item->sName}}</td>
                                <td class="center-align">{{$item->email}}</td>
                                <td class="center-align">{{$item->club}}</td>
                                <td class="center-align">{{$item->house}}</td>
                                <td class="center-align">{{$item->gender}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </main>
        </div>

        <div id="primary5" class="mn-content fixed-sidebar">
            <main class="mn-inner">
                <div  class="card upload" >

                    <br>
                    <span class="mytabs1"><a href="{{url('/admins/view-students-in-PrimaryFive')}}">A</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryFive')}}#primary5B">B</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimaryFive')}}#primary5C">C</a></span>

                    <div class="card-content">
                        <span class="card-title">Primary 5</span><br>
                            <table class="bordered table-striped">
                                <tr>
                                    <th class="center-align">Name</th>
                                    <th class="center-align">Email</th>
                                    <th class="center-align">Club</th>
                                    <th class="center-align">House</th>
                                    <th class="center-align">Gender</th>
                                </tr>
                                @foreach($primaryFives as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->oName}} {{$item->sName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->club}}</td>
                                    <td class="center-align">{{$item->house}}</td>
                                    <td class="center-align">{{$item->gender}}</td>
                                </tr>
                                @endforeach
                            </table>
                    </div>
                </div>
            </main>
        </div>

        <div id="primary6" class="mn-content fixed-sidebar">
            <main class="mn-inner">
                <div  class="card upload" >

                    <br>
                    <span class="mytabs1"><a href="{{url('/admins/view-students-in-PrimarySix')}}">A</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimarySix')}}#primary6B">B</a></span>
                    <span class="mytabs"><a href="{{url('/admins/view-students-in-PrimarySix')}}#primary6C">C</a></span>

                    <div class="card-content">
                        <span class="card-title">Primary 6</span><br>
                            <table class="bordered table-striped">
                                <tr>
                                    <th class="center-align">Name</th>
                                    <th class="center-align">Email</th>
                                    <th class="center-align">Club</th>
                                    <th class="center-align">House</th>
                                    <th class="center-align">Gender</th>
                                </tr>
                                @foreach($primarySixs as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->oName}} {{$item->sName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->club}}</td>
                                    <td class="center-align">{{$item->house}}</td>
                                    <td class="center-align">{{$item->gender}}</td>
                                </tr>
                                @endforeach
                            </table>
                    </div>
                </div>
            </main>
        </div>

    </div>


@endsection