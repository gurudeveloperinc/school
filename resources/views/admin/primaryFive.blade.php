@extends('layouts.app')

@section('content')

    @include('sidebars.admin')

    <div class="row">

        <div class="col m1 right goBack">
            <a href="javascript:history.go(-1)" title="Go to previous page"><i class="material-icons circle white-text grey right">arrow_back</i></a>
        </div>

        <div class="col m4 right" id="centerIt">
            <ul class="tabs tabs-transparent uploadOptions">
                <li class="tab"><a class="inactive" href="#primary5A">A</a></li>
                <li class="tab"><a class="inactive" href="#primary5B">B</a></li>
                <li class="tab"><a class="inactive" href="#primary5C">C</a></li>
            </ul>
        </div>
    </div>

    <div class="formBody">

        <div id="primary5A" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">
                        <span class="card-title">Primary 5A</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Club</th>
                                <th class="center-align">House</th>
                                <th class="center-align">Gender</th>
                            </tr>
                            @foreach($primaryFiveA as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->oName}} {{$item->sName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->club}}</td>
                                    <td class="center-align">{{$item->house}}</td>
                                    <td class="center-align">{{$item->gender}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </main>
        </div>

        <div id="primary5B" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">
                        <span class="card-title">Primary 5B</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Club</th>
                                <th class="center-align">House</th>
                                <th class="center-align">Gender</th>
                            </tr>
                            @foreach($primaryFiveB as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->oName}} {{$item->sName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->club}}</td>
                                    <td class="center-align">{{$item->house}}</td>
                                    <td class="center-align">{{$item->gender}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </main>
        </div>

        <div id="primary5C" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">
                        <span class="card-title">Primary 5C</span><br>
                        <table class="bordered table-striped">
                            <tr>
                                <th class="center-align">Name</th>
                                <th class="center-align">Email</th>
                                <th class="center-align">Club</th>
                                <th class="center-align">House</th>
                                <th class="center-align">Gender</th>
                            </tr>
                            @foreach($primaryFiveC as $item)
                                <tr>
                                    <td class="center-align">{{$item->fName}} {{$item->oName}} {{$item->sName}}</td>
                                    <td class="center-align">{{$item->email}}</td>
                                    <td class="center-align">{{$item->club}}</td>
                                    <td class="center-align">{{$item->house}}</td>
                                    <td class="center-align">{{$item->gender}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </main>
        </div>
    </div>

@endsection