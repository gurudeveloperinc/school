@extends('layouts.app')

@section('content')

    @include('sidebars.admin')

    <div class="row">

        <div class="col m10 right">
            <ul class="tabs tabs-transparent uploadOptions">
                <li class="tab"><a class="active" href="#singleUpload">Single Upload</a></li>
                <li class="tab"><a href="#bulkUpload">Bulk Upload</a></li>

            </ul>
        </div>

        @if(isset($status))
            @if( $status == "Staff added successfully!")
                <div class="col m10 right success card-panel green" align="center">{{$status}}</div>
            @else
                <div class="col m10 right error card-panel red" align="center">{{$status}}</div>
            @endif
        @endif
    </div>


    <div class="formBody">
        <div id="singleUpload" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">
                        <span class="card-title">Add Staff</span><br>

                        <form method="post" action="{{url('admins/add-staff')}}">

                            {{csrf_field()}}

                            <div class="input-field col s6">
                                <input  id="fName" name="fName" type="text" class="validate" required>
                                <label for="fName">First Name</label>
                            </div>

                            <div class="input-field col s6">
                                <input  id="sName" name="sName" type="text" class="validate" required>
                                <label for="sName">Surname</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="oName" name="oName" type="text" class="validate">
                                <label for="oName">Other Names</label>
                            </div>

                            <div class="input-field col s6">
                                <input  id="email" name="email" type="email" class="validate" required>
                                <label for="email">Email</label>
                            </div>

                            <div class="input-field col s6">
                                <input id="userName" name="userName" type="text" class="validate" required>
                                <label for="userName">User Name</label>
                            </div>

                            <div class="input-field col s6">
                                <input  id="password" name="password" type="password" class="validate" required>
                                <label for="password">Password</label>
                            </div>

                            <div class="input-field col s6">
                                <input  id="phone" name="phone" type="text" class="validate" >
                                <label for="phone">Phone Number</label>
                            </div>

                            <div class="input-field col s6">
                                <input  id="dob" name="dob" type="text" class="validate" required>
                                <label for="dob">Date of Birth</label>
                            </div>

                            <div class="input-field col s6">
                                <input  id="address" name="address" type="text" class="validate" >
                                <label for="address">Address</label>
                            </div>

                            <div class="input-field col s6">
                                <input  id="stateOfOrigin" name="stateOfOrigin" type="text" class="validate" required>
                                <label for="stateOfOrigin">State Of Origin</label>
                            </div>

                            <div class="input-field col s6">
                                <input  id="nextOfKin" name="nextOfKin" type="text" class="validate" required>
                                <label for="nextOfKin">Next of Kin's Name</label>
                            </div>

                            <div class="input-field col s6">
                                <input  id="nextOfKinPhone" name="nextOfKinPhone" type="text" class="validate" >
                                <label for="nextOfKinPhone">Next of Kin's Phone Number</label>
                            </div>


                            <div class="row" style="text-align: center">

                                <div class="select-wrapper">
                                    <span class="caret"></span>
                                    <select name="gender" id="gender">
                                        <option value="" disabled="" selected="">Select the Gender:</option>
                                        <option value="1">MALE</option>
                                        <option value="2">FEMALE</option>
                                    </select>
                                </div>

                                <div class="select-wrapper">
                                    <span class="caret"></span>
                                    <select id="department" name="department">
                                        <option value="" disabled="" selected="selected">Select the department:</option>
                                        <option value="Finance"> Finance </option>
                                        <option value="Bursar"> Bursar </option>
                                        <option value="ICT"> ICT </option>
                                        <option value="HR"> HR </option>
                                        <option value="Academic"> Academic </option>
                                    </select>
                                </div>

                                <div class="select-wrapper">
                                    <span class="caret"></span>
                                    <select id="maritalStatus" name="maritalStatus">
                                        <option value="" disabled="" selected="selected">Select your marital Status:</option>
                                        <option value="Married">Married</option>
                                        <option value="Single">Single</option>
                                        <option value="Divorced">Divorced</option>
                                        <option value="Widowed">Widowed</option>

                                    </select>
                                </div>




                            </div>

                            <button class="btn waves-effect waves-light" type="submit" name="action" id="adminBarOne">Submit
                                <i class="material-icons right">send</i>
                            </button>


                            <button class="btn waves-effect waves-light red" type="reset" name="action" id="adminBarTwo">Clear
                                <i class="material-icons right" >clear</i>
                            </button>

                        </form>
                    </div>
                </div>
            </main>


        </div>
        <div id="bulkUpload" class="mn-content fixed-sidebar">

            <main class="mn-inner">
                <div  class="card upload" >
                    <div class="card-content">

                        <span class="card-title">BULK STAFF UPLOAD FORM</span><br>
                        <a class="right" href="#">Download sample here</a> <br>

                        <span class="card-title">UPLOAD FILE</span><br>

                        <div class="card-content" id="uploadHelp">
                            <form method="post" enctype="multipart/form-data" action="{{url('/admins/bulk-add-staff')}}" class="dropzone">
                                {{csrf_field()}}
                                <div class="fallback">
                                    <input name="file" type="file" />
                                </div>
                            </form>

                            <br>

                        </div>

                    </div>
                </div>
            </main>

        </div>

        <div class="left-sidebar-hover"></div>


        <script src="{{url('assets/plugins/dropzone/dropzone.min.js')}}"></script>
        <script src="{{url('assets/plugins/dropzone/dropzone-amd-module.min.js')}}"></script>
    </div>


@endsection