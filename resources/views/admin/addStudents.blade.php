@extends('layouts.app')

@section('content')

    @include('sidebars.admin')

    <div class="row">

        <div class="col m10 right">
            <ul class="tabs tabs-transparent uploadOptions">
                <li class="tab"><a class="active" href="#singleUpload">Single Upload</a></li>
                <li class="tab"><a href="#bulkUpload">Bulk Upload</a></li>

            </ul>
        </div>

        @if(isset($status))
            @if( $status == "Student added successfully!")
                 <div class="col m10 right success card-panel green" align="center">{{$status}}</div>
            @else
                 <div class="col m10 right error card-panel red" align="center">{{$status}}</div>
            @endif
        @endif
    </div>


<div class="formBody">
    <div id="singleUpload" class="mn-content fixed-sidebar">

        <main class="mn-inner">
            <div  class="card upload" >
                <div class="card-content">
                    <span class="card-title">Add a Student</span><br>

                    <form method="post" action="{{url('admins/add-students')}}">

                        {{csrf_field()}}

                        <div class="input-field col s6">
                            <input  id="fName" name="fName" type="text" class="validate" required>
                            <label for="fName">First Name</label>
                        </div>


                        <div class="input-field col s6">
                            <input  id="sName" name="sName" type="text" class="validate" required>
                            <label for="sName">Surname</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="oName" name="oName" type="text" class="validate">
                            <label for="oName">Other Names</label>
                        </div>

                        <div class="input-field col s6">
                            <input  id="dob" name="dob" type="text" class="validate" required>
                            <label for="dob">Date of Birth</label>
                        </div>

                        <div class="input-field col s6">
                            <input id="userName" name="userName" type="text" class="validate" required>
                            <label for="userName">User Name</label>
                        </div>

                        <div class="input-field col s6">
                            <input  id="password" name="password" type="password" class="validate" required>
                            <label for="password">Password</label>
                        </div>

                        <div class="input-field col s6">
                            <input  id="email" name="email" type="email" class="validate" required>
                            <label for="email">Email</label>
                        </div>


                        <div class="input-field col s6">
                            <input  id="club" name="club" type="text" class="validate" >
                            <label for="club">Club</label>
                        </div>

                        <div class="input-field col s6">
                            <input  id="stateOfOrigin" name="stateOfOrigin" type="text" class="validate" required>
                            <label for="stateOfOrigin">State Of Origin</label>
                        </div>

                        <div class="input-field col s6">
                            <input  id="nextOfKin" name="nextOfKin" type="text" class="validate" required>
                            <label for="nextOfKin">Next of Kin's Name</label>
                        </div>

                        <div class="input-field col s6">
                            <input  id="nextOfKinPhone" name="nextOfKinPhone" type="text" class="validate" >
                            <label for="nextOfKinPhone">Next of Kin's Phone Number</label>
                        </div>


                        <div class="row" style="text-align: center">

                            <div class="select-wrapper">
                                <span class="caret"></span>
                                <select id="class" name="class">
                                    <option value="" disabled="" selected="selected">Select the Class:</option>
                                    <option value="Primary 1">Primary One</option>
                                    <option value="Primary 2">Primary Two</option>
                                    <option value="Primary 3">Primary Three</option>
                                    <option value="Primary 4">Primary Four</option>
                                    <option value="Primary 5">Primary Five</option>
                                    <option value="Primary 6">Primary Six</option>
                                </select>
                            </div>

                            <div class="select-wrapper">
                                <span class="caret"></span>
                                <select id="classExtension" name="classExtension">
                                    <option value="" disabled="" selected="selected">Select the Extension:</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                </select>
                            </div>


                            <div class="select-wrapper">
                                <span class="caret"></span>
                                <select id="house" name="house">
                                    <option value="" disabled="" selected="selected">Select the House:</option>
                                    <option value="blue">blue</option>
                                    <option value="green">green</option>
                                    <option value="yellow">yellow</option>
                                    <option value="red">red</option>

                                </select>
                            </div>


                            <div class="select-wrapper">
                                <span class="caret"></span>
                                <select name="gender" id="gender">
                                    <option value="" disabled="" selected="">Select the Gender:</option>
                                    <option value="1">MALE</option>
                                    <option value="2">FEMALE</option>
                                </select>
                            </div>

                        </div>

                        <button class="btn waves-effect waves-light" type="submit" name="action" id="adminBarOne">Submit
                            <i class="material-icons right">send</i>
                        </button>


                        <button class="btn waves-effect waves-light red" type="reset" name="action" id="adminBarTwo">Clear
                            <i class="material-icons right" >clear</i>
                        </button>

                    </form>

                </div>
            </div>
        </main>


    </div>
    <div id="bulkUpload" class="mn-content fixed-sidebar">

        <main class="mn-inner">
            <div  class="card upload" >
                <div class="card-content">

                    <span class="card-title">BULK STUDENT UPLOAD FORM</span><br>
                    <a class="right" href="#">Download sample here</a> <br>

                    <span class="card-title">UPLOAD FILE</span><br>

                    <div class="card-content" id="uploadHelp">
                        <form method="post" enctype="multipart/form-data" action="{{url('/admins/bulk-add-students')}}" class="dropzone">
                            {{csrf_field()}}
                            <div class="fallback">
                                <input name="file" type="file" />
                            </div>
                        </form>

                        <br>

                    </div>

                </div>
            </div>
        </main>

    </div>

    <div class="left-sidebar-hover"></div>


    <script src="{{url('assets/plugins/dropzone/dropzone.min.js')}}"></script>
    <script src="{{url('assets/plugins/dropzone/dropzone-amd-module.min.js')}}"></script>
</div>


@endsection