@extends('layouts.app')

@section('content')
    @include('sidebars.admin')


    <div class="row">
        <div class="col m10 right">
            <div class="categoryBody"><br>
                    <p class="categoryTitle">Please select a category</p>

                    <ul>
                        <li><a href="{{url('/admins/view-students-class')}}">Class</a></li>
                        <li><a href="#">House</a></li>
                        <li><a href="#">Club</a></li>
                    </ul>

            </div>
        </div>
    </div>

@endsection