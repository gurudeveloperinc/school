@extends('layouts.auth')

@section('content')

<link rel="stylesheet" href="{{URL::asset('assets/css/lbis.css')}}">

    <div class="mn-content valign-wrapper ">
        <main class="mn-inner container ">
            <div class="valign">
                <div class="row">

                    <div class="col s12 m6 l6 offset-l2" >
                        <div class="card white darken-1">
                            <div class="card-content z-depth-5 loginForm">
                                <span class="card-title"><img class = "logoProfile" src="assets/images/logo.png"></span>
                                <span class="card-title teal-text">LBIS</span>
                                <span class="card-title" id="subtleWhite">Log In</span>
                                <div class="row login">
                                    <form class="col s12" role="form" method="POST" action="{{ url('/login') }}">
                                        {{ csrf_field() }}

                                        @if ($errors->has('email'))
                                            <div class="error" align="center">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </div>
                                        @endif

                                        <div class="input-field s12 {{ $errors->has('email') ? ' has-error' : '' }}">

                                            <label for="email" id="subtleGrey">Staff ID</label>
                                            <input id="email" type="text" class="validate" name="email" value="{{ old('email') }}" required autofocus>

                                        </div>

                                        @if ($errors->has('password'))
                                            <span class="error">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                        <div class="input-field s12  {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 control-label" id="subtleGrey">Password</label>

                                            <input id="password" type="password" class="form-control" name="password" required>


                                        </div>

                                        <div class="input-field s12 ">
                                            <div class="col-md-6 col-md-offset-4">
                                                <input type="checkbox" id="remember" name="remember" />
                                                <label for="remember" id="subtleGrey">Remember Me</label>
                                            </div>
                                        </div>

                                        <div class="input-field s12 ">
                                            <div class="col-md-8 col-md-offset-4">

                                                <a href="{{ url('/password/reset') }}" id="subtleWhite">
                                                    Forgot Your Password?
                                                </a>

                                            </div>
                                        </div>
                                        <br>
                                        <div class="col s12 right-align m-t-sm">
                                            <button type="submit" class="btn btn-primary teal">
                                                Login
                                            </button>

                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>

    </div>


@endsection
